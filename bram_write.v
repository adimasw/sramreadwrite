`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    07:12:52 05/24/2018 
// Design Name: 
// Module Name:    bram_write 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bram_write(
    output reg[17:0]d,
    input write_ce,
    input [71:0] wr_data,
    input [18:0] address,
	 output reg[18:0] sa_w,
	 output w_n,
	 input clk,
	 input k_p,
	 output reg wr_busy
    );
reg wr_en_n=1;
assign w_n=wr_en_n;
reg[2:0] wr_state=0;
always @(posedge clk)
begin
	if (|wr_state)
	begin
		wr_state<=wr_state+1;
		case (wr_state)
		3'h1:
		begin
		end
		3'h2:
		begin
			d<=wr_data[17:0];			
		end
		3'h3:
		begin
			d<=wr_data[35:18];
			wr_en_n<=1;
		end
		3'h4:
		begin
			d<=wr_data[53:36];
		end
		3'h5:
		begin
			d<=wr_data[71:54];
		end
		3'h6:
		begin
			wr_busy<=0;
		end
		endcase
	end
	else
	begin
		sa_w<=address;
		if (write_ce&k_p)
		begin
			wr_busy<=1;
			wr_state<=3'h1;
			wr_en_n<=0;
		end
	end
end

endmodule
