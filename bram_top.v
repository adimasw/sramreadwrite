`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:06:08 05/24/2018 
// Design Name: 
// Module Name:    bram_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bram_top(
    input system_clk_p,
    input system_clk_n,
    input [17:0] qdriip_q,
    output [18:0] qdriip_sa,
    output [17:0] qdriip_d,
    output qdriip_r_n,
    output qdriip_w_n,
    output [1:0] qdriip_bw_n,
    output qdriip_k_n,
    output qdriip_k_p,
	 input qdriip_cq_n,
	 input qdriip_cq_p,
	 input qdriip_qvld,
	 output qdriip_dll_off_n
    );

wire[35:0] ctrl;
wire[71:0] data_out;
wire [71:0]data_in=72'hA6A6A6A6A6A6A6A6A6;//72'h02030405060708090a;//72'h010203040506070809;
wire clk;
assign qdriip_bw_n=2'h0;
reg write_ce=0;
wire wr_busy;
reg read_ce=0;
wire rd_busy;
reg[18:0] addr=19'h0;
reg[2:0] state=0;
wire [18:0]sa_r;
wire [18:0]sa_w;
assign qdriip_sa=wr_busy?sa_w:sa_r;
assign qdriip_dll_off_n=1;
reg [12:0]count;

bram_write write(.d(qdriip_d),.write_ce(write_ce),.wr_data(data_in),.address(addr),.sa_w(sa_w),.w_n(qdriip_w_n),.clk(clk),.k_p(qdriip_k_p),.wr_busy(wr_busy));
bram_read read(.q(qdriip_q),.read_ce(read_ce),.rd_data(data_out),.address(addr),.sa_r(sa_r),.r_n(qdriip_r_n),.clk(clk),.k_p(qdriip_k_p),.rd_busy(rd_busy),.cq_n(qdriip_cq_n),.cq_p(qdriip_cq_p),.qvld(qdriip_qvld));
icon ICON(.CONTROL0(ctrl));
vio VIO(.CONTROL(ctrl),.ASYNC_IN(data_out));
clockgen clkgen(.CLK_IN1_P(system_clk_p),.CLK_IN1_N(system_clk_n),.CLK_OUT1(qdriip_k_p),.CLK_OUT2(qdriip_k_n),.CLK_OUT3(clk));
always @(posedge clk)
begin
	case (state)
	3'h0:
	begin
		if (&count)
			state<=3'h1;
		else
			count<=count+1;
	end		
	3'h1:
	begin
		write_ce<=1;
		state<=3'h2;
	end
	3'h2:
	begin
	if (wr_busy==1)
		begin
			write_ce<=0;
			state<=3'h3;
		end
	end
	3'h3:
	begin
		if (wr_busy==0) 
		begin
			state<=3'h4;
		end
	end
	3'h4:
	begin
		read_ce<=1;
		if (rd_busy==1)
			state<=3'h5;
	end
	3'h5:
	begin
		read_ce<=0;
	end
	endcase
end
endmodule
