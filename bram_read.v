`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:50:48 05/24/2018 
// Design Name: 
// Module Name:    bram_read 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bram_read( input [17:0]q,
    input read_ce,
    output[71:0] rd_data,
    input[18:0] address,
	 output reg[18:0] sa_r,
	 output r_n,
	 input clk,
	 input k_p, 
	 input cq_p,
	 input cq_n,
	 input qvld,
	 output reg rd_busy
    );
reg rd_en_n=1;
assign r_n=rd_en_n;
reg [4:0]rd_state=0;
reg pos_state=0;
reg neg_state=0;
reg [35:0]pos_data;
reg [35:0]neg_data;
assign rd_data[17:0] = neg_data[17:0];
assign rd_data[35:18]=pos_data[17:0];
assign rd_data[53:36]=neg_data[35:18];
assign rd_data[71:54]=pos_data[35:18];
reg qvld_delayed=0;
always @(posedge clk)
begin
	if (|rd_state==1)
	begin
	case (rd_state)
	4'h1:
		begin
			rd_state<=4'h2;
		end
	4'h2:
	begin
		rd_en_n<=1;
		if (qvld==1)
			rd_state<=4'h3;
	end
	4'h3:
	begin
		if (qvld==0)
			rd_state<=4'h4;
	end
	4'h4:
		begin
			rd_state<=0;
			rd_busy<=0;
		end
	endcase
	end
	else
	begin
	if ((read_ce==1) & (k_p==1))
		begin
			rd_state<=1;
			rd_busy<=1;
			sa_r<=address;
			rd_en_n<=0;
		end
	end
end
always @(posedge cq_p)
begin
	qvld_delayed<=qvld;
	if (!pos_state&qvld_delayed)
	begin
		pos_data[17:0]<=q;
		pos_state<=1;
	end
	if (pos_state)
	begin
		pos_data[35:18]<=q;
		pos_state<=0;
	end
end
always @(posedge cq_n)
begin
	if (!neg_state&qvld)
		begin
			neg_data[17:0]<=q;
			neg_state<=1;
		end
		if (neg_state==1)
		begin
			neg_data[35:18]<=q;
			neg_state<=0;
		end
end
endmodule
