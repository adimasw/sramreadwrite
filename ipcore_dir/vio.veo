///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor     : Xilinx
// \   \   \/     Version    : 14.2
//  \   \         Application: Xilinx CORE Generator
//  /   /         Filename   : vio.veo
// /___/   /\     Timestamp  : Wed May 30 06:51:43 SE Asia Standard Time 2018
// \   \  /  \
//  \___\/\___\
//
// Design Name: ISE Instantiation template
///////////////////////////////////////////////////////////////////////////////

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
vio YourInstanceName (
    .CONTROL(CONTROL), // INOUT BUS [35:0]
    .ASYNC_IN(ASYNC_IN) // IN BUS [71:0]
);

// INST_TAG_END ------ End INSTANTIATION Template ---------

